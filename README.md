# Terraform Blog Post

Terraform code example used in https://darecode.com/blog/infraestructura-como-codigo-con-terraform/

## Resources

This example creates the following resources in AWS:

* 1xVPC
* 1xSubnet
* 2xEc2 instances
* 1xRDS MySQL DB
