provider "aws" { 
  region = "eu-west-1" 
} 

resource "aws_vpc" "projectvpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  
  tags = {
	Name = "projectvpc"
  }
}

resource "aws_subnet" "projectsubnet" {
  vpc_id     = aws_vpc.projectvpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "projectsubnet"
  }

}

resource "aws_instance" "frontend" {
  ami           = "ami-0ac80df6eff0e70b5"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.projectsubnet.id

  tags = {
	Name = "Frontend"
  }
}

resource "aws_instance" "backend" {
  ami           = "ami-0ac80df6eff0e70b5"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.projectsubnet.id

  tags = {
	Name = "Backend"
  }
}

resource "aws_db_instance" "projectdb" {    
	allocated_storage    = 20
	storage_type         = "gp2"
	engine               = "mysql"
	engine_version       = "8.0.19"
	instance_class       = "db.t2.micro"
	name                 = "projectdb"
	username             = "foo"
	password             = "foobarbaz"
	parameter_group_name = "default.mysql8.0"
	skip_final_snapshot = true
} 
